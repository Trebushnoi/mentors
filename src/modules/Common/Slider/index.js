import { Slider, WhiteSlider } from './Slider.js';
import { SliderWithTabs } from './SliderWithTabs.js';

export { Slider, WhiteSlider, SliderWithTabs }
