import React, { Component } from 'react';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';
import { baseCss } from '../StyledComponent';
import { media } from '../Media';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import  LeftIcon  from './Left.svg';

class SliderWithTabs extends Component {
  state = {
      currentIndex: 0
  }

  constructor() {
      super();

      this.onSelect = this.onSelect.bind(this)
      this.clickOnLeft = this.clickOnLeft.bind(this);
      this.clickOnRight = this.clickOnRight.bind(this);
  }

  render() {
      const { slideContent, ContentComponent, className } = this.props;
      const params = {
          pagination: {
              el: '.swiper-pagination',
              clickable: true
          }
      };

      this.setsOfContent = slideContent.reduce((finalArray, data, index) => {
          if (index % 3 === 0) {
              finalArray.push([data]);
          } else {
              finalArray[finalArray.length - 1].push(data)
          }

          return finalArray;
      }, []);

      return (
        <div className = {className}>
            <Swiper {...params}>
                {slideContent.map((slide, index) => {
                    return <div key = {index}>
                        <ContentComponent {...slide} />
                    </div>
                })}
            </Swiper>

            <div className = 'usual-list'>
                <Tabs className = 'tabs' selectedIndex = {this.state.currentIndex} onSelect = {this.onSelect}>
                    <TabList>
                        <div className = 'dots'>
                            {this.setsOfContent.map((tab, index) => {
                                return <Tab key = {index}> <div className = 'controll'></div></Tab>
                            })}
                        </div>
                        <div className = 'pointers'>
                            <img onClick = {this.clickOnLeft} className = 'pointer pointer_left' src = {LeftIcon} />
                            <img onClick = {this.clickOnRight} className = 'pointer pointer_right' src = {LeftIcon} />
                        </div>

                    </TabList>

                    {this.setsOfContent.map((set, index) => {
                        return <TabPanel key = { index }>
                            {set.map((content, index) => {
                                return <div className = "content-component-wrap" key = {index}>
                                    <ContentComponent {...content} />
                                </div>
                            })}
                        </TabPanel>
                    })}
                </Tabs>
            </div>
        </div>
      );
  }

  clickOnLeft() {
      const tabLength = this.setsOfContent.length;
      let newCurrentIndex;

      if (tabLength === this.state.currentIndex + 1) {
          newCurrentIndex = 0;
      } else {
          newCurrentIndex = this.state.currentIndex + 1;
      }

      this.setState({
          currentIndex: newCurrentIndex
      });
  }

  clickOnRight() {
      const tabLength = this.setsOfContent.length;
      let newCurrentIndex;

      if (0 === this.state.currentIndex) {
          newCurrentIndex = tabLength - 1;
      } else {
          newCurrentIndex = this.state.currentIndex - 1;
      }

      this.setState({
          currentIndex: newCurrentIndex
      });
  }

  onSelect(index) {
      this.setState({
          currentIndex: index
      });
  }
}

const StyledSliderWithTabs = styled(SliderWithTabs)`${baseCss}`.extend`
    .swiper-slide {
        flex-shrink: 0;
        position: relative;
        // padding-left: 3px;
        // padding-right: 3px;
        box-sizing: border-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-wrapper {
        position: relative;
        width: 100%;
        // height: 100%;
        z-index: 1;
        display: flex;
        box-sizing: content-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-slide-active {
        margin-right: 15px;
    }

    .swiper-slide-prev {
        padding-right: 15px;
    }

    .swiper-container {
        margin-left: auto;
        margin-right: auto;
        position: relative;
        // overflow: hidden;
        z-index: 1;
        ${media.medium`
            display: none;
        `}
    }

    .swiper-pagination {
        display:flex;
        justify-content: center;
        margin-top:20px;
    }

    .swiper-pagination-bullet {
          height: 22px;
          width: 22px;

          border-radius: 50%;
          position: relative;
          box-sizing: border-box;
          margin-right: 7px;
          &:last-child {
            margin-right: 0;
          }

          &:after {
              position: absolute;
              content: '';
              height: 10px;
              width: 10px;
              background-color: white;
              border-radius: 50%;
              top: calc(50% - 5px);
              left: calc(50% - 5px);
          }
    }

    .swiper-pagination-bullet-active {
        border: 2px solid white;
    }

    .usual-list {
        display: none;

        ${media.medium`
            display: flex;
        `}
    }

    .list-item {
        flex: 1;
        margin-right: 30px;
        &:last-child {
          margin-right: 0;
        }
    }


    /////

    .controll {
        height: 22px;
        width: 22px;

        border-radius: 50%;
        position: relative;
        box-sizing: border-box;
        cursor: pointer;

        &:after {
            position: absolute;
            content: '';
            height: 10px;
            width: 10px;
            background-color: white;
            border-radius: 50%;
            top: calc(50% - 5px);
            left: calc(50% - 5px);
        }
    }

    .react-tabs__tab {
      margin-right: 7px;
      &:last-child {
        margin-right: 0;
      }
    }

    .react-tabs__tab--selected {
        .controll {
            border: 2px solid white;
        }
    }

    .react-tabs__tab-list {
        // display: inline-flex;
        position: absolute;
        right: 0;
        top: -57px;

        ${media.large`
            top: -85px;
        `}
    }

    .dots {
        display: inline-flex;
        vertical-align: middle;
        margin-right: 30px;
    }

    .tabs {
        padding: 0;
        position: relative;
        ${media.medium`
            padding: 0;
        `}
    }

    .pointers {
        display: inline-block;
        vertical-align: middle;
    }

    .pointer {
        cursor: pointer;
    }

    .pointer_left {
        margin-right: 20px;

    }

    .pointer_right {
        transform: rotate(180deg);
    }

    .react-tabs__tab-panel {
        display: flex;

        .content-component-wrap {
            flex: 1;
            margin-right: 30px;
            &:last-child {
              margin-right: 0;
            }
        }
    }
`;

export { StyledSliderWithTabs as SliderWithTabs };
