import React, { Component } from 'react';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';
import { baseCss } from '../StyledComponent';
import { media } from '../Media';

class Slider extends Component {

  render() {
      const { slideContent, ContentComponent, className } = this.props;
      const params = {
          pagination: {
              el: '.swiper-pagination',
              clickable: true
          }
      };

      return (
        <div className = {className}>
            <Swiper {...params}>
                {slideContent.map((slide, index) => {
                    return <div key = {index}>
                        <ContentComponent {...slide} />
                    </div>
                })}
            </Swiper>

            <div className = 'usual-list'>
                {slideContent.map((tab, index) => {
                    return <div className = 'list-item' key = {index}>
                        <ContentComponent {...tab} />
                    </div>
                })}
            </div>

        </div>
      );
  }
}

const StyledSlider = styled(Slider)`${baseCss}`.extend`
    .swiper-slide {
        flex-shrink: 0;
        position: relative;
        padding-left: 3px;
        padding-right: 3px;
        box-sizing: border-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-wrapper {
        position: relative;
        width: 100%;
        height: 100%;
        z-index: 1;
        display: flex;
        box-sizing: content-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-slide-active {
        margin-right: 15px;
    }

    .swiper-slide-prev {
        padding-right: 15px;
    }

    .swiper-container {
        margin-left: auto;
        margin-right: auto;
        position: relative;
        // overflow: hidden;
        z-index: 1;
        ${media.medium`
            display: none;
        `}
    }

    .swiper-pagination {
        display:flex;
        justify-content: center;
        margin-top:20px;
    }

    .swiper-pagination-bullet {
          height: 22px;
          width: 22px;

          border-radius: 50%;
          position: relative;
          box-sizing: border-box;
          margin-right: 7px;
          &:last-child {
            margin-right: 0;
          }

          &:after {
              position: absolute;
              content: '';
              height: 10px;
              width: 10px;
              background-color: #27AE60;
              border-radius: 50%;
              top: calc(50% - 5px);
              left: calc(50% - 5px);
          }
    }

    .swiper-pagination-bullet-active {
        border: 2px solid rgba(39, 174, 96, 0.5);
    }

    .usual-list {
        display: none;

        ${media.medium`
            display: flex;
        `}
    }

    .list-item {
        flex: 1;
        margin-right: 30px;
        &:last-child {
          margin-right: 0;
        }
    }
`;


const WhiteSlider = styled(Slider)`${baseCss}`.extend`
    .swiper-slide {
        flex-shrink: 0;
        position: relative;
        // padding-left: 3px;
        // padding-right: 3px;
        box-sizing: border-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-wrapper {
        position: relative;
        width: 100%;
        // height: 100%;
        z-index: 1;
        display: flex;
        box-sizing: content-box;
        transition-property: transform,-webkit-transform;
    }

    .swiper-slide-active {
        margin-right: 15px;
    }

    .swiper-slide-prev {
        padding-right: 15px;
    }

    .swiper-container {
        margin-left: auto;
        margin-right: auto;
        position: relative;
        // overflow: hidden;
        z-index: 1;
        ${media.medium`
            display: none;
        `}
    }

    .swiper-pagination {
        display:flex;
        justify-content: center;
        margin-top:20px;
    }

    .swiper-pagination-bullet {
          height: 22px;
          width: 22px;

          border-radius: 50%;
          position: relative;
          box-sizing: border-box;
          margin-right: 7px;
          &:last-child {
            margin-right: 0;
          }

          &:after {
              position: absolute;
              content: '';
              height: 10px;
              width: 10px;
              background-color: white;
              border-radius: 50%;
              top: calc(50% - 5px);
              left: calc(50% - 5px);
          }
    }

    .swiper-pagination-bullet-active {
        border: 2px solid white;
    }

    .usual-list {
        display: none;

        ${media.medium`
            display: flex;
        `}
    }

    .list-item {
        flex: 1;
        margin-right: 30px;
        &:last-child {
          margin-right: 0;
        }
    }
`;

export { StyledSlider as Slider, WhiteSlider };
