import React, { Component } from 'react';
import styled from 'styled-components';
import { baseCss } from '../StyledComponent';
import { media } from '../Media';

class Button extends Component {
  render() {
    const { className } = this.props;

    return (
      <button className = {className}>
          <span className = 'button_text'>{this.props.text}</span>
      </button>
    );
  }
}

const StyledButton = styled(Button)`${baseCss}`.extend`
    width: 100%;
    height: 100%;
    background: white;
    box-shadow: 0px 6px 4px rgba(0, 0, 0, 0.1);
    border: none;
    font-family: 'Roboto Regular';
    font-size: 18px;
    line-height: 21px;
    color: #27AE60;
    padding: 10px 0;
    border-radius: 100px;
    outline: none;

    ${media.medium`
        font-size: 22px;
        line-height: 26px;
        &:hover {
            box-shadow: none;
            background-color: #27AE60;
            color: white;
        }
    `}
`;

const LightButton = styled(Button)`${baseCss}`.extend`
    width: 100%;
    height: 100%;
    background: white;
    border: 2px solid #27AE60;
    letter-spacing: 1px;
    font-family: 'Roboto Bold';
    font-size: 12px;
    line-height: 14px;
    text-transform: uppercase;
    color: #27AE60;
    padding: 10px 0;
    border-radius: 100px;
    outline: none;

    ${media.medium`
        font-size: 16px;
        line-height: 19px;
        &:hover {
            background: #27AE60;
            color: white;
        }
    `}
`;

const GreenButton = styled(Button)`${baseCss}`.extend`
    width: 100%;
    background: #27AE60;
    color: white;
    box-shadow: ${props => props.theme.boxShadow};
    padding: 10px 0;
    border-radius: 100px;
    font-family: 'Roboto Regular';
    font-size: 18px;
    line-height: 21px;
    outline: none;
    border: none;

    &:hover {
        box-shadow: none;
    }

    ${media.medium`
        font-size: 22px;
        line-height: 26px;
    `}

    ${media.large`
        font-size: 24px;
        line-height: 28px;
    `}
`;

GreenButton.defaultProps = {
    theme: {
        boxShadow: '0px 6px 4px rgba(0, 0, 0, 0.1)'
    }
}

const SimpleButton = styled(Button)`${baseCss}`.extend`
    padding: 0;
    border: none;
    background: transparent;
    font-family: 'Roboto Bold';
    font-size: 12px;
    line-height: 14px;
    letter-spacing: 1px;
    color: #27AE60;
    text-transform: uppercase;

    ${media.medium`
        font-size: 16px;
        line-height: 19px;
    `}
`;



export { StyledButton as Button, LightButton, GreenButton, SimpleButton }
