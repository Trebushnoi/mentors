import React, { Component } from 'react';
import styled from 'styled-components';
import { baseCss } from '../StyledComponent';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { media } from '../Media';

const classNames = {
    SELECTED_TAB: 'react-tabs__tab--selected',
    TAB_LIST: 'react-tabs__tab-list',
}

class AdvancedTabs extends Component {
  constructor() {
      super();

      this.onSelect = this.onSelect.bind(this)
  }

  render() {
    const { tabsContent, ContentComponent, className, injectedParams } = this.props;


    return (
      <Tabs className = {className} onSelect = {this.onSelect}
          domRef={(tabElement) => { this.tabElement = tabElement; }}>
              <div className = 'tab-list'>
                  <TabList>
                      {tabsContent.map((tab, index) => {
                          return <Tab key = {index}> {tab.title} </Tab>
                      })}
                      <div className = 'illumination' ref={(illumination) => { this.illumination = illumination; }}></div>
                  </TabList>
              </div>
              {tabsContent.map((tab, index) => {
                  return <TabPanel key = {index}>
                      {tab.list.map((content, index) => {
                          return <div className = 'content'  key = {index}>
                              <ContentComponent {...content} {...injectedParams}/>
                          </div>
                      })}
                  </TabPanel>
              })}
      </Tabs>
    );
  }

  componentDidMount() {
      this.getTabListElement();
      this.setInitialWidthOfFirstSelected()
  }

  onSelect(index, lastIndex, event) {
      this.updateIllumination(event.target);
  }

  updateIllumination(selectedTab) {
    this.setIlluminationWidth(getComputedStyle(selectedTab));
    this.setIlluminationShift(this.getIlluminationShift(selectedTab));
  }

  getTabListElement() {
      this.tabList = this.tabElement.getElementsByClassName(classNames.TAB_LIST)[0];
  }

  getSelectedTabElement() {
      return this.tabElement.getElementsByClassName(classNames.SELECTED_TAB)[0]
  }

  getIlluminationShift(selectedTab) {
    let coordinatesOfTabList = this.tabList.getBoundingClientRect();
    let coordinatesOfSelectedTab = selectedTab.getBoundingClientRect();

    return coordinatesOfSelectedTab.left - coordinatesOfTabList.left;
  }

  setInitialWidthOfFirstSelected() {
      this.setIlluminationWidth(getComputedStyle(this.getSelectedTabElement()));
      this.setIlluminationTransition();
  }

  setIlluminationWidth({width}) {
      this.illumination.style.width = width;
  }

  setIlluminationShift(shift) {
      this.illumination.style.left = shift + 'px';
  }

  setIlluminationTransition() {
      this.illumination.style.transition = 'all 200ms';
  }
}

const StyledTabs = styled(AdvancedTabs)`${baseCss}`.extend`
    width: calc(100% + 30px);
    margin-left: -15px;
    border-bottom: 1px solid #E8E8E8;

    .tab-list {
        border-bottom: 1px solid #E8E8E8;
    }

    .illumination {
        display: none;

        ${media.medium`
            display: block;
            height: 3px;
            width: 0px;
            background-color: #27AE60;
            position: absolute;
            left: 0;
            bottom: 0;
        `}
    }

    .react-tabs__tab-list {
        display: flex;
        overflow-x: auto

        font-family: 'Roboto Bold';
        font-size: 12px;
        line-height: 14px;
        color: #2F80ED;
        letter-spacing: 1px;

        ${media.medium`
            max-width: 720px;
            margin:auto;
            font-size: 16px;
            line-height: 19px;
            position: relative;
            overflow-x: visible;
        `}

        ${media.large`
            max-width: 940px;
        `}

        ${media.wide`
            max-width: 1146px;
        `}
    }

    .react-tabs__tab {
        margin-right: 40px;
        cursor: pointer;
        padding-bottom: 20px;
        flex-shrink: 0;
        &:first-child {
            padding-left: 15px;
            ${media.medium`
                padding-left: 0;
            `}
        }
        &:last-of-type {
            padding-right: 15px;
            ${media.medium`
                padding-right: 0;
            `}
        }

        ${media.medium`
            margin-right: 50px;
        `}
    }

    .react-tabs__tab--selected {
        color: #212121;
        position:relative;
        &:after {
            position: absolute;
            content: '';
            bottom: 0px;
            height: 3px;
            width: 100%;
            background-color: #27AE60;
        }

        &:first-child, &:last-child {
            &:after {
                position: absolute;
                content: '';
                bottom: 0px;
                height: 3px;
                width: calc(100% - 15px);
                background-color: #27AE60;

                ${media.medium`
                    width: 100%;
                `}
            }
        }

        ${media.medium`
            &:after {
                display: none;
            }
        `}
    }

    .react-tabs__tab-panel {
        display: flex;
        overflow-x: auto;

        ${media.medium`
            max-width: 720px;
            margin:auto;
            overflow-x: visible;
        `}

        ${media.large`
            max-width: 940px;
        `}

        ${media.wide`
            max-width: 1146px;
        `}

    }

    .content {
        min-width: 200px;
        margin-right: 20px;

        &:last-child {
          margin-right: 0;
        }

        ${media.medium`
            flex: 0 0 226px;
        `}

        ${media.large`
            margin-right: 40px;
            flex: 0 0 287px;
            &:last-child {
              margin-right: 0;
            }
        `}

        ${media.wide`
            flex: 0 0 335px;
        `}

    }
`;

export { StyledTabs as AdvancedTabs }
