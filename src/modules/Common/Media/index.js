import { mediaQuery } from 'styled-media-queries';

export const breakpoint = {
    medium:    '768px',
    large:     '1180px',
    wide:      '1380px'
};

export const media = {
    medium:   mediaQuery`screen and (min-width: ${breakpoint.medium})`,
    large:    mediaQuery`screen and (min-width: ${breakpoint.large})`,
    wide:     mediaQuery`screen and (min-width: ${breakpoint.wide})`
};
