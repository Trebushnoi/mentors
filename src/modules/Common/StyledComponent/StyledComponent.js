import styled, { css } from 'styled-components';
import * as React from 'react';

// interface ExternalStyles {
//     margin?: string;
//     flexShrink?: string;
//     flexGrow?: string;
//     width?: string;
//     height?: string;
//     display?: string;
// }
//
// interface Props<THEME = void, THEME_TYPE = string> {
//     externalStyles?: ExternalStyles;
//     className?: string;
//     theme?: THEME;
//     themeType?: THEME_TYPE;
//     children?: React.ReactNode|React.ReactNode[];
// }

const baseCss = css`
    margin: ${({externalStyles}: Props) => ((externalStyles && externalStyles.margin) ? externalStyles.margin : '0')};
    ${({externalStyles}: Props) => (
        (externalStyles && externalStyles.flexShrink) ? `flex-shrink: ${externalStyles.flexShrink};` : ''
    )}
    ${({externalStyles}: Props) => (
        (externalStyles && externalStyles.flexGrow) ? `flex-grow: ${externalStyles.flexGrow};` : ''
    )}
    ${({externalStyles}: Props) => (
        (externalStyles && externalStyles.width) ? `width: ${externalStyles.width};` : ''
    )}
    ${({externalStyles}: Props) => (
        (externalStyles && externalStyles.height) ? `height: ${externalStyles.height};` : ''
    )}
    ${({externalStyles}: Props) => (
        (externalStyles && externalStyles.display) ? `display: ${externalStyles.display};` : ''
    )}
`;

const StyledComponent = styled.div`${baseCss}`;

export { StyledComponent, Props, baseCss };
