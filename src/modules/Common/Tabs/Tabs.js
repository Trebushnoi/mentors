import React, { Component } from 'react';
import styled from 'styled-components';
import { baseCss } from '../StyledComponent';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { media } from '../Media';
import Swipeable from 'react-swipeable';

class TabsComponent extends Component {

  state = {
      currentIndex: 0
  }

  constructor() {
      super();

      this.onSelect = this.onSelect.bind(this)
      this.swipingLeft = this.swipingLeft.bind(this);
      this.swipingRight = this.swipingRight.bind(this);
  }

  render() {
    const { tabsContent, ContentComponent, className } = this.props;

    return (
      <div className = {className}>
          <Tabs className = 'tabs'
            selectedIndex = {this.state.currentIndex} onSelect = {this.onSelect}>
              {tabsContent.map((tab, index) => {
                  return <TabPanel key = {index}>
                      <Swipeable onSwipingLeft={this.swipingLeft} onSwipingRight={this.swipingRight} delta = {50} >
                          <ContentComponent {...tab} />
                      </Swipeable>
                  </TabPanel>
              })}

              <TabList>
                  {tabsContent.map((tab, index) => {
                      return <Tab key = {index}> <div className = 'controll'>  </div> </Tab>
                  })}
              </TabList>
          </Tabs>

          <div className = 'usual-list'>
              {tabsContent.map((tab, index) => {
                  return <div className = 'list-item' key = {index}>
                      <ContentComponent {...tab} />
                  </div>
              })}
          </div>
      </div>
    );
  }

  onSelect(index) {
      this.setState({
          currentIndex: index
      });
  }

  swipingLeft() {
      const tabLength = this.props.tabsContent.length;
      let newCurrentIndex;

      if (tabLength === this.state.currentIndex + 1) {
          newCurrentIndex = 0;
      } else {
          newCurrentIndex = this.state.currentIndex + 1;
      }

      this.setState({
          currentIndex: newCurrentIndex
      });
  }

  swipingRight() {
      const tabLength = this.props.tabsContent.length;
      let newCurrentIndex;

      if (0 === this.state.currentIndex) {
          newCurrentIndex = tabLength - 1;
      } else {
          newCurrentIndex = this.state.currentIndex - 1;
      }

      this.setState({
          currentIndex: newCurrentIndex
      });
  }
}

const StyledTabs = styled(TabsComponent)`${baseCss}`.extend`
    .react-tabs__tab-list {
        display:flex;
        justify-content: center;
    }

    .react-tabs__tab {
        padding-right: 14px;
        padding-top: 20px;
        &:last-child {
            padding-right: 0;
        }
    }

    .controll {
        height: 22px;
        width: 22px;

        border-radius: 50%;
        position: relative;
        box-sizing: border-box;

        &:after {
            position: absolute;
            content: '';
            height: 10px;
            width: 10px;
            background-color: #27AE60;
            border-radius: 50%;
            top: calc(50% - 5px);
            left: calc(50% - 5px);
        }
    }

    .react-tabs__tab--selected {
        .controll {
            border: 2px solid rgba(39, 174, 96, 0.5);
        }
    }

    .tabs {
        ${media.medium`
            display: none;
        `}
    }

    .usual-list {
        display: none;

        ${media.medium`
            display: flex;
            // padding: ${props => props.theme.padding || '0 15px'};
        `}
    }

    .list-item {
        flex: 1;
        margin-right: 30px;
        &:last-child {
          margin-right: 0;
        }
    }

`;

const WhiteTabs = styled(TabsComponent)`${baseCss}`.extend`
    .react-tabs__tab-list {
        margin-top: 20px;
        display:flex;
        justify-content: center;
    }

    .react-tabs__tab {
      height: 22px;
      width: 22px;

      border-radius: 50%;
      margin-right: 14px;
      position: relative;
      box-sizing: border-box;

      &:after {
          position: absolute;
          content: '';
          height: 10px;
          width: 10px;
          background-color: white;
          border-radius: 50%;
          top: calc(50% - 5px);
          left: calc(50% - 5px);
      }
    }

    .react-tabs__tab--selected {
        border: 2px solid white;
    }

    .tabs {
        ${media.medium`
            display: none;
        `}
    }

    .usual-list {
        display: none;

        ${media.medium`
            display: flex;
        `}
    }

    .list-item {
        flex: 1;
        margin-right: 15px;
        &:last-child {
          margin-right: 0;
        }

        ${media.large`
            margin-right: 30px;
        `}
    }
`;

export { StyledTabs as TabsComponent, WhiteTabs }
