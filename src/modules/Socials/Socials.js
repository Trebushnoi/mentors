import React, { Component } from 'react';
import { baseCss } from '../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { media } from '../Common/Media';

class Socials extends Component {
  render() {
    return (
      <Wrapper>
          <Social name = 'Вконтакте' href = 'https://vk.com/mentorforces' />
          <ThemeProvider theme = {{ background: '#3B5998' }}>
              <Social name = 'Facebook' href='http://fb.me/mentorforces'/>
          </ThemeProvider>
          <ThemeProvider theme = {{ background: '#3CADF0' }}>
              <Social name = 'Twitter' href='https://twitter.com/mentorforces'/>
          </ThemeProvider>
          <ThemeProvider theme = {{ background: '#8A3AB9' }}>
              <Social name = 'Instagram' href='http://instagram.com/mentorforces'/>
          </ThemeProvider>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    ${media.medium`
        display: flex;
    `}
`;

const Social = ({name, href}) => (
   <SocialWrapper href = {href} target = '_blank'>
      <Text> {name} </Text>
   </SocialWrapper>
);

const SocialWrapper = styled.a`${baseCss}`.extend`
    background: ${props => props.theme.background};
    display: flex;
    align-items: center;
    justify-content: center;
    height: 60px;

    &:hover {
        opacity: .7;
    }

    ${media.medium`
        flex: 1;
        height: 80px;
    `}

    ${media.large`
        height: 100px;
    `}

`;

SocialWrapper.defaultProps = {
    theme: {
        background: '#5181B8'
    }
}

const Text = styled.span`${baseCss}`.extend`
    color: white;
    font-family: 'Proxima Nova Regular';
    font-size: 20px;
    line-height: 23px;

    ${media.medium`
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        font-size: 30px;
        line-height: 35px;
    `}
`;

export { Socials }
