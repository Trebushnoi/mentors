import React, { Component } from 'react';
import pointer from '../Common/Icons/pointer.svg';
import pointerHover from '../Common/Icons/pointer_hover.svg';
import { SimpleMentor as Mentor } from '../Mentors/Mentor';
import { data } from './data';
import styled, { ThemeProvider } from 'styled-components';
import { baseCss } from '../Common/StyledComponent';
import { AdvancedTabs } from '../Common/AdvancedTabs';
import { SimpleButton } from '../Common/Button';
import { media } from '../Common/Media';

class Directions extends Component {
  render() {

    return (
      <Wrapper>
          <TitleWrapper>
              <Title> Какое направление вас интересует? </Title>
              <a target='_blank' href='http://mentorforces.com/mentors'  className = 'more'>
                  <span className = 'full-list'> Все направления </span>
                  <img className = 'pointer' src = { pointer } />
                  <img className = 'pointer_hover' src = { pointerHover } />
              </a>
          </TitleWrapper>

          <AdvancedTabs injectedParams = {{ ButtonComponent: SimpleButton }} tabsContent = { data }
              ContentComponent = { Mentor } />
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px 0px;

    ${media.medium`
        padding: 50px 0 0;
    `}

    ${media.large`
        padding: 70px 0 0;
    `}

    .more {
      .pointer_hover {
        display: none;
      }

      .pointer {
          padding-left: 15px;
          ${media.medium`
              padding-left: 0;
          `}
      }

      ${media.medium`
          &:hover {
              .full-list {
                  color: rgba(47, 128, 237, 0.7);
              }

              .pointer {
                  display: none;
              }

              .pointer_hover {
                display: inline-block;
              }
          }
      `}

    }
`;

const TitleWrapper = styled.div`${baseCss}`.extend`
    display: flex;
    justify-content: space-between;
    padding-bottom: 30px;
    align-items: flex-start;
    ${media.medium`
        max-width: 720px;
        margin: auto;
        padding-bottom: 30px;
    `}

    ${media.large`
        max-width: 940px;
        align-items: center;
        padding-bottom: 50px;
    `}

    ${media.wide`
        max-width: 1146px;
    `}

    .full-list {
        display: none;
        ${media.medium`
            display: inline-block;
            vertical-align: middle;
            color: #2F80ED;
            font-size: 16px;
            line-height: 25px;
            font-family: 'Roboto Regular';
            margin-right: 20px;
        `}

        ${media.large`
          font-size: 18px;
          line-height: 28px;
        `}
    }

    .pointer, .pointer_hover {
        ${media.medium`
            display: inline-block;
            vertical-align: middle;
        `}

        ${media.large`
            width: 30px;
        `}
    }
`;

const Title = styled.h2`${baseCss}`.extend`
    font-family: 'Proxima Nova Bold';
    font-size: 20px;
    line-height: 23px;
    max-width: 222px;
    color: #212121;

    ${media.medium`
        font-size: 28px;
        max-width: 350px;
        line-height: 33px;
    `}

    ${media.large`
      font-size: 30px;
      line-height: 35px;
      max-width: none;
    `}
`;

export { Directions }
