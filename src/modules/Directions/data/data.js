import photo from './photo.jpg';

//Mobile
import dmitryChuta from './dmitry-chuta.jpg';
import dmitryZhukov from './dmitry-zhukov.jpg';
import alexeyKozhevnikov from './alexey-kozhevnikov.jpg';
import nikitaYarigin from './nikita-yarygin.jpg';

//WEB
import arthurKasimov from './arthur-kasimov.jpg';

//UI
import artemTaradash from './artem-taradash.jpg';
import andreyLisitsyn from './andrey-lisitsyn.png';

//UX
import alexanderMartynov from './alexander-martynov.jpg';
import alexanderKatin from './alexander-katin.jpg';

//Motion
import pavelPazukhin from './pavel-pazukhin.jpg';
import igorBatrakov from './igor-batrakov.jpg';

//Illus
import nazirovIlnur from './nazirov-ilnur.jpg';

export const data = [
    {
        title: 'MOBILE',
        list: [
            {
                name: 'Дмитрий Чута',
                post: 'Арт-директор',
                star: '5',
                photo: dmitryChuta,
                link: 'https://mentorforces.com/mentors/dmitrij-chuta'
            },
            {
                name: 'Никита Ярыгин',
                post: 'Дизайнер продукта',
                star: '5',
                photo: nikitaYarigin,
                link: 'https://mentorforces.com/mentors/nikita-jarygin'
            },
            {
                name: 'Алексей Кожевников',
                post: 'Дизайнер, Арт-директор',
                star: '5',
                photo: alexeyKozhevnikov,
                link: 'https://mentorforces.com/mentors/aleksej-kozhevnikov'
            }
        ]
    },
    {
        title: 'WEB',
        list: [
            {
                name: 'Алексей Кожевников',
                post: 'Дизайнер, Арт-директор',
                star: '5',
                photo: alexeyKozhevnikov,
                link: 'https://mentorforces.com/mentors/aleksej-kozhevnikov'
            },
            {
                name: 'Артур Касимов',
                post: 'Senior product designer',
                star: '5',
                photo: arthurKasimov,
                link: 'https://mentorforces.com/mentors/artur-kasimov'
            },
            {
                name: 'Дмитрий Жуков',
                post: 'Арт-директор, Дизайнер',
                star: '5',
                photo: dmitryZhukov,
                link: 'https://mentorforces.com/mentors/dmitrij-zhukov'
            }
        ]
    },
    {
        title: 'UI',
        list: [
            {
                name: 'Дмитрий Жуков',
                post: 'Арт-директор, Дизайнер',
                star: '5',
                photo: dmitryZhukov,
                link: 'https://mentorforces.com/mentors/dmitrij-zhukov'
            },
            {
                name: 'Артем Тарадаш',
                post: 'Дизайнер директор',
                star: '5',
                photo: artemTaradash,
                link: 'https://mentorforces.com/mentors/artem-taradash'
            },
            {
                name: 'Андрей Лисицын',
                post: 'Арт-директор',
                star: '5',
                photo: andreyLisitsyn,
                link: 'https://mentorforces.com/mentors/andrej-isicyn'
            }
        ]
    },
    {
        title: 'UX',
        list: [
            {
                name: 'Александр Мартынов',
                post: 'Дизайнер, Арт-директор',
                star: '5',
                photo: alexanderMartynov,
                link: 'https://mentorforces.com/mentors/aleksandr-martynov'
            },
            {
                name: 'Александр Катин',
                post: 'Дизайнер в sympli.io',
                star: '5',
                photo: alexanderKatin,
                link: 'https://mentorforces.com/mentors/aleksandr-katin'
            },
            {
                name: 'Александр Богомолов',
                post: 'Старший UX-дизайнер',
                star: '5',
                link: 'https://mentorforces.com/mentors/aleksandr-bogomolov',
                photo
            }
        ]
    },
    {
        title: 'MOTION DESGIN',
        list: [
            {
                name: 'Павел Пазухин',
                post: 'Старший UX-дизайнер',
                star: '5',
                photo: pavelPazukhin,
                link: 'https://mentorforces.com/mentors/pavel-pazuhin'
            },
            {
                name: 'Игорь Батраков',
                post: 'Арт-директор',
                star: '5',
                photo: igorBatrakov,
                link: 'https://mentorforces.com/mentors/igor-batrakov'
            }
        ]
    },
    {
        title: 'ИЛЛЮСТРАЦИЯ',
        list: [
            {
                name: 'Александр Мартынов',
                post: 'Дизайнер, Арт-директор',
                star: '5',
                photo: alexanderMartynov,
                link: 'https://mentorforces.com/mentors/aleksandr-martynov'
            },
            {
                name: 'Илнур Назыров',
                post: 'Дизайнер, Иллюстратор',
                star: '5',
                photo: nazirovIlnur,
                link: 'https://mentorforces.com/mentors/ilnur-nazyrov'
            }
        ]
    }
]
