import photo from './photo.jpg';

import alexeyKozhevnikov from './alexey-kozhevnikov.jpg';
import artemTaradash from './artem-taradash.jpg';
import arthurKasimov from './arthur-kasimov.jpg';

export const data = {
    mentors: [
      {
          name: 'Артур Касимов',
          post: 'Senior product designer',
          star: '5',
          photo: arthurKasimov,
          link: 'https://mentorforces.com/mentors/artur-kasimov'
      },
      {
          name: 'Артем Тарадаш',
          post: 'Дизайнер директор',
          star: '5',
          photo: artemTaradash,
          link: 'https://mentorforces.com/mentors/artem-taradash'
      },
      {
          name: 'Алексей Кожевников',
          post: 'Дизайнер, Арт-директор',
          star: '5',
          photo: alexeyKozhevnikov,
          link: 'https://mentorforces.com/mentors/aleksej-kozhevnikov'
      }
    ]
};
