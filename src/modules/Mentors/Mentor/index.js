import { Mentor } from './Mentor.js';
import { DefaultMentor } from './theme/DefaultMentor.js';
import { SimpleMentor } from './theme/SimpleMentor.js';
import { OpinionMentor } from './theme/OpinionMentor.js';

export { DefaultMentor as Mentor, SimpleMentor, OpinionMentor }
