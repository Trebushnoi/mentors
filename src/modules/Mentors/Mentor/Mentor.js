import React from 'react';
import starIcon from './star.svg';
import { baseCss } from '../../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { LightButton as Button } from '../../Common/Button';

const Mentor = ({photo, name, star, post, opinion, link, className, ButtonComponent}) => {
  const CurrentButtonComponent = ButtonComponent  || Button;
  return (
      <div className = {className} >
          <div className = 'media-wrapper'>
              { photo && <img className = 'photo' src = { photo } /> }
              <div className = 'star'>
                  <div className = 'star-icon'/>
                  <span className = 'star-value'> {star} </span>
              </div>
          </div>
          <div className = 'content'>
              <h4 className = 'name'> {name} </h4>
              <p className = 'post'> {post} </p>

              {opinion &&
                  <p className = 'opinion'> {opinion} </p>}

              <a href = {link} className = 'button'>
                  <CurrentButtonComponent text = 'Забронировать'/>
              </a>
          </div>
      </div>
 )};


export { Mentor }
