import { Mentor } from '../Mentor.js';
import styled, { ThemeProvider } from 'styled-components';
import { baseCss } from '../../../Common/StyledComponent';
import { media } from '../../../Common/Media';

const SimpleMentor = styled(Mentor)`${baseCss}`.extend`
    background-color: white;
    padding: 40px 15px;

    &:hover {
        .button_text {
            color: rgba(39, 174, 96, 0.7);
        }
    }

    ${media.medium`
        padding: 50px 0;
    `}

    ${media.large`
        display: flex;
        padding: 70px 0;
    `}

    .photo {
        display: none;

        ${media.large`
            display: block;
            border-radius: 50%;
            width: 50px;
            margin-right: 30px;
        `}

        ${media.wide`
            margin-top: -8px;
            width: 70px;
        `}
    }

    .star {
        display: none;
    }

    .name {
        font-family: 'Roboto Bold';
        font-size: 16px;
        line-height: 19px;
        color: #212121;

        ${media.medium`
            font-size: 18px;
            line-height: 21px;
        `}
    }

    .post {
        font-family: 'Roboto Light';
        font-size: 14px;
        line-height: 16px;
        color: #212121;
        margin: 10px 0 0 0;

        ${media.medium`
            font-size: 16px;
            line-height: 19px;
        `}
    }

    .button {
        margin: 20px 0 0;
        display: block;
        
        ${media.medium`
            margin: 30px 0 0;
        `}
    }
`;

export { SimpleMentor };
