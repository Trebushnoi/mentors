import { Mentor } from '../Mentor.js';
import styled, { ThemeProvider } from 'styled-components';
import { baseCss } from '../../../Common/StyledComponent';
import { media } from '../../../Common/Media';
import starIcon from '../star.svg';

const DefaultMentor = styled(Mentor)`${baseCss}`.extend`
    background-color: white;
    text-align: center;
    padding: 35px 15px 15px;
    border-radius: 10px;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);

    ${media.medium`
        padding: 40px 0px;
    `}

    .photo {
        display: inline-block;
        border-radius: 50%;
        vertical-align: middle;
        margin: 0 40px 0 0;

        ${media.medium`
            display: block;
            margin: auto;
            width: 130px;
        `}

        ${media.wide`
            width: 170px;
        `}
    }

    .star {
        display: inline-block;
        vertical-align: middle;

        ${media.medium`
            display: block;
            margin-top: 20px;
        `}
    }

    .star-icon {
        background-image: url(${starIcon});
        width: 18px;
        height: 17px;
    }

    .star-icon, .star-icon_hover {
        display: inline-block;
        vertical-align: middle;
        margin: 0 10px 0 0;
    }

    .star-value {
        display: inline-block;
        font-family: 'Roboto Bold';
        font-size: 16px;
        line-height: 19px;
        vertical-align: middle;

        ${media.medium`
            font-size: 18px;
            line-height: 21px;
        `}
    }

    .name {
        font-family: 'Roboto Bold';
        font-size: 16px;
        line-height: 19px;
        color: #212121;
        margin: 20px 0 0 0;

        ${media.medium`
            font-size: 18px;
            line-height: 21px;
        `}
    }

    .post {
        font-family: 'Roboto Light';
        font-size: 14px;
        line-height: 16px;
        color: #212121;
        margin: 10px 0 0 0;

        ${media.medium`
            font-size: 16px;
            line-height: 19px;
        `}
    }

    .button {
        margin: 40px 0 0;
        display: block;
        ${media.medium`
            width: 210px;
            height: 44px;
            margin: 40px auto 0;
        `}
    }
`;

export { DefaultMentor };
