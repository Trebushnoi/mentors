import { Mentor } from '../Mentor.js';
import styled, { ThemeProvider } from 'styled-components';
import { baseCss } from '../../../Common/StyledComponent';
import { media } from '../../../Common/Media';

const OpinionMentor = styled(Mentor)`${baseCss}`.extend`
    background-color: white;
    padding: 15px 15px 52px;
    border-radius: 6px;

    ${media.medium`
        padding: 20px 15px 47px;
        height: 100%;
        box-sizing: border-box;
        min-height: 477px;
    `}

    ${media.large`
        padding: 30px 30px 22px;
    `}

    ${media.wide`
        padding: 30px 30px 47px;
        min-height: 430px;
    `}

    .photo {
        display: none;
    }

    .star {
        display: none;
    }

    .name {
        font-family: 'Roboto Bold';
        font-size: 16px;
        line-height: 1;
        color: #212121;

        ${media.medium`
            font-size: 18px;
        `}
    }

    .post {
        font-family: 'Roboto Light';
        font-size: 14px;
        line-height: 1;
        color: #212121;
        margin: 10px 0 0 0;

        ${media.medium`
            font-size: 16px;
        `}
    }

    .button {
        display: none;
    }

    .opinion {
        margin-top: 30px;
        font-family: 'Roboto Light';
        font-size: 14px;
        line-height: 21px;
        color: #212121;

        ${media.medium`
            font-size: 16px;
            line-height: 24px;
        `}

        ${media.large`
            margin-top: 40px;
        `}
    }
`;

export { OpinionMentor };
