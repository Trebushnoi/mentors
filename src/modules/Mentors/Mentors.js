import React, { Component } from 'react';
import pointer from '../Common/Icons/pointer.svg';
import pointerHover from '../Common/Icons/pointer_hover.svg';
import { Slider } from '../Common/Slider';
import { data } from './data';
import { Mentor } from './Mentor';
import { baseCss } from '../Common/StyledComponent';
import styled from 'styled-components';
import { media } from '../Common/Media';

class Mentors extends Component {
  render() {
    return (
      <Wrapper>
          <TitleWrapper>
              <Title> Лучшие менторы </Title>
              <a target='_blank' href='http://mentorforces.com/mentors' className = 'more'>
                  <span className = 'full-list'> Полный список менторов </span>
                  <img className = 'pointer' src = { pointer } />
                  <img className = 'pointer_hover' src = { pointerHover } />
              </a>
          </TitleWrapper>
          <div className = 'tabs-mentors'>
              <Slider slideContent = {data.mentors} ContentComponent = { Mentor } />
          </div>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;
    background-color: #F9F9F9;

    .more {
        .pointer_hover {
          display: none;
        }

        .full-list {
            display: none;

            ${media.medium`
                display: inline-block;
                vertical-align: middle;
                color: #2F80ED;
                font-size: 16px;
                line-height: 25px;
                font-family: 'Roboto Regular';
                margin-right: 20px;
            `}

            ${media.large`
                font-size: 18px;
                line-height: 28px;
            `}
        }

        .pointer {
            padding-left: 15px;
            ${media.medium`
                padding-left: 0;
            `}
        }

        ${media.medium`
            &:hover {
                .full-list {
                    color: rgba(47, 128, 237, 0.7);
                }

                .pointer {
                    display: none;
                }

                .pointer_hover {
                  display: inline-block;
                }
            }
        `}
    }

    .pointer, .pointer_hover {
        ${media.medium`
            display: inline-block;
            vertical-align: middle;
        `}

        ${media.large`
            width: 30px;
        `}
    }

    ${media.medium`
        padding: 50px 0;
      }
    `}

    ${media.large`
        padding: 70px 0;
    `}

    .tabs-mentors {
      ${media.medium`
          padding: 0 15px;
      `}

      ${media.large`
          padding: 0;
          max-width: 940px;
          margin: auto;
      `}

      ${media.wide`
          max-width: 1146px;
      `}
    }
`;

const TitleWrapper = styled.div`${baseCss}`.extend`
    display: flex;
    justify-content: space-between;
    padding-bottom: 30px;
    align-items: center;

    ${media.medium`
        max-width: 720px;
        margin: auto;
    `}

    ${media.large`
        max-width: 940px;
        padding-bottom: 50px;
    `}

    ${media.wide`
        max-width: 1146px;
    `}
`;

const Title = styled.h2`${baseCss}`.extend`
    font-family: 'Proxima Nova Bold';
    font-size: 20px;
    line-height: 23px;

    ${media.medium`
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        font-size: 30px;
        line-height: 35px;
    `}
`;

export { Mentors }
