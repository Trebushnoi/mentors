import React, { Component } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { baseCss } from '../Common/StyledComponent';
import { media } from '../Common/Media';

class Workflow extends Component {
  render() {
    const data = [
        'Выбираете ментора, длину консультации, а также дату и время консультации',
        'Оплачиваете консультацию удобным для вас способом',
        'В указанное время с вами связывается ментор',
    ];

    return (
      <Wrapper>
          <Title> Как это работает? </Title>

          <WrapperForItems>
              {data.map((item, index) =>
                <WorkflowItem key = {index} index = { index + 1 } name = { item } />)}
          </WrapperForItems>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;
    border-top: 1px solid #E8E8E8;

    ${media.medium`
        max-width: 720px;
        margin: auto;
        padding: 50px 0px;
        border-top: none;
    `}

    ${media.large`
        max-width: 940px;
        padding: 70px 0px;
    `}

    ${media.wide`
        max-width: 1146px;
    `}

    .workflow-item {
        margin-top: 40px;
        &:first-child {
          margin-top: 0;
        }

        &:last-child {
          ${media.large`
              align-items: baseline;
          `}
        }

        &:nth-child(2) {
          ${media.wide`
              align-items: baseline;
          `}
        }

        ${media.medium`
            display: flex;
            padding-bottom: 0;
            margin-top: 50px;
        `}

        ${media.large`
            margin: 70px 0 0;
        `}
    }
`;

const WrapperForItems = styled.div`${baseCss}`.extend`
    margin: 30px 0 0;

    ${media.large`
        margin: 70px 0 0;
    `}
`;

const Title = styled.h3`${baseCss}`.extend`
    color: #212121;
    font-family: 'Proxima Nova Bold';
    font-size: 20px;
    line-height: 23px;

    ${media.medium`
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        font-size: 30px;
        line-height: 35px;
    `}
`;

const WorkflowItem = ({index, name}) => (
   <div className = 'workflow-item'>
      <Index> {index} </Index>
      <Text> {name} </Text>
   </div>
);

const Index = styled.span`${baseCss}`.extend`
    border: 3px solid #FF9000;
    border-radius: 50%;
    height: 33px;
    box-sizing: border-box;
    display: block;
    width: 33px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Proxima Nova Bold';
    font-size: 16px;
    line-height: 1;
    color: #212121;

    ${media.medium`
        margin-right: 44px;
        flex-shrink: 0;
        width: 51px;
        height: 51px;
        font-size: 28px;
        margin-top: 14px;
    `}

    ${media.large`
        font-size: 30px;
    `}

    ${media.wide`
        margin-right: 40px;
    `}
`;

const Text = styled.p`${baseCss}`.extend`
    font-family: 'Proxima Nova Regular';
    font-size: 22px;
    line-height: 26px;
    color: #212121;
    margin-top: 20px;

    position: relative;
    padding-bottom: 15px;

    &: after {
        position: absolute;
        bottom: 0;
        height: 1px;
        width: 100vw;
        background-color: #E8E8E8;
        content: '';
        left: 0;
    }

    ${media.medium`
        margin-top: 0;
        padding-bottom: 30px;
        font-size: 32px;
        line-height: 38px;
    `}

    ${media.large`
        font-size: 34px;
        line-height: 40px;
        width: 100%;
    `}

    ${media.wide`
        max-width: 878px;
    `}
`;

export { Workflow }
