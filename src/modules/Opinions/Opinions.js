import React, { Component } from 'react';
import { OpinionMentor as Mentor } from '../Mentors/Mentor';
import { data } from './data';
import { baseCss } from '../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { WhiteTabs as TabsComponent } from '../Common/Tabs';
import { media } from '../Common/Media';
import image from './Ico.svg';
import imageLarge from './Ico-large.svg';
import imageWide from './Ico-wide.svg';
import { SliderWithTabs } from '../Common/Slider';

class Opinions extends Component {
  render() {
    return (
      <Wrapper>
          <Title> Отзывы </Title>
          <div className = 'tabs-opinions'>
              <SliderWithTabs slideContent = {data.mentors} ContentComponent = { Mentor } />
          </div>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;
    background-color: #27AE60;

    ${media.medium`
        padding: 50px 0;
        background-image: url('${image}');
        background-repeat: no-repeat;
        background-position: right;
        background-size: 120%;
    `}

    ${media.large`
        padding: 70px 0;

        background-image: url('${imageLarge}');
        background-size: 93%;
    `}

    ${media.wide`
        background-image: url('${imageWide}');
        background-size: 91%;
    `}

    .tabs-opinions {
        ${media.medium`
            padding: 0 15px;
        `}

        ${media.large`
            padding: 0;
            max-width: 940px;
            margin: auto;
        `}

        ${media.wide`
            max-width: 1146px;
        `}
    }
`;

const Title = styled.h3`${baseCss}`.extend`
    font-family: 'Proxima Nova Bold';
    font-size: 20px;
    line-height: 23px;
    color: white;
    padding-bottom: 30px;

    ${media.medium`
        max-width: 720px;
        margin: auto;
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        max-width: 940px;
        font-size: 30px;
        line-height: 35px;
        padding-bottom: 50px;
    `}

    ${media.wide`
        max-width: 1146px;
    `}
`;

export { Opinions }
