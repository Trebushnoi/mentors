import React, { Component } from 'react';
import { data } from './data.js';
import { baseCss } from '../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { GreenButton as Button } from '../Common/Button';
import { media } from '../Common/Media';
import { Slider } from '../Common/Slider';

class Prices extends Component {
  render() {
    return (
      <Wrapper>
          <Title> Сколько стоит консультация ментора? </Title>
          <Slider slideContent = {data.prices} ContentComponent = { Price } />
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;

    ${media.medium`
        padding: 50px 0px 0px;
        max-width: 720px;
        margin: auto;
    `}

    ${media.large`
        padding: 70px 0;
        max-width: 940px;
    `}

    ${media.wide`
        max-width: 1146px;
    `}
`;

const Title = styled.h3`${baseCss}`.extend`
    font-family: 'Proxima Nova Bold';
    font-size: 20px;
    line-height: 23px;
    color: #212121;
    padding-bottom: 30px;

    ${media.medium`
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        font-size: 30px;
        line-height: 35px;
        padding-bottom: 50px;
    `}
`;


const Price = ({price, time}) => (
   <PriceWrapper>
     <a target='_blank' href='http://mentorforces.com/mentors'>
          <PriceValue> <span className = 'price-value'> { price } </span> </PriceValue>
          <Time> { time } </Time>
          <div className = 'button'>
              <ThemeProvider theme = {{ boxShadow: 'none' }}>
                  <Button text = 'Получить консультацию'/>
              </ThemeProvider>
          </div>
    </a>
   </PriceWrapper>
);

const PriceWrapper = styled.div`${baseCss}`.extend`
    border-radius: 6px;
    border: 1px solid #E8E8E8;
    padding: 15px;

    ${media.medium`
        transition: all 200ms;
        &:hover {
            transform: scale(1.03);
            .price-value {
                color: #27AE60;
            }
        }
    `}

    ${media.medium`
        padding: 20px;
    `}

    ${media.large`
        padding: 30px;
    `}

    .button {
        margin-top: 30px;

        ${media.medium`
            margin-top: 50px;
        `}

        ${media.large`
            margin-top: 100px;
        `}
    }
`;

const PriceValue = styled.span`${baseCss}`.extend`
    font-family: 'Proxima Nova Bold';
    font-size: 24px;
    line-height: 28px;
    display: block;
    color: #212121;

    ${media.medium`
        font-size: 30px;
        line-height: 1;
    `}

    ${media.large`
        font-size: 48px;
        line-height: 56px;
    `}
`;

const Time = styled.span`${baseCss}`.extend`
    font-family: 'Proxima Nova Regular';
    font-size: 20px;
    line-height: 28px;
    display: block;
    color: rgba(33, 33, 33, 0.5);
    margin: 10px 0 0;

    ${media.medium`
        font-size: 25px;
        line-height: 1;
        margin: 20px 0 0;
    `}

    ${media.large`
        font-size: 30px;
        line-height: 35px;
    `}
`;

export { Prices }
