import React, { Component } from 'react';
import { GreenButton as Button } from '../Common/Button';
import { baseCss } from '../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { media } from '../Common/Media';

class Mail extends Component {
  render() {
    return (
      <Wrapper>
          <form action="https://mentorforces.us18.list-manage.com/subscribe/post?u=b3292b86d7774ac1272312505&amp;id=7fb4347bfe" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
              <Title> Остались вопросы? </Title>
              <Paragraph> Оставьте свой e-mail. Мы с вами свяжемся и ответим
    на все вопросы. </Paragraph>
              <Input type="email" name="EMAIL" id="mce-EMAIL" placeholder = 'Введите свой e-mail'/>
              <div style={{position: 'absolute',  left: '-5000px'}} aria-hidden="true"><input type="text" name="b_b3292b86d7774ac1272312505_7fb4347bfe" tabIndex="-1" value=""/></div>
              <div className = 'button'>
                  <Button text = 'Отправить' />
              </div>
          </form>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;

    ${media.medium`
        padding: 54px 0 50px;
        text-align: center;
    `}

    ${media.large`
        padding: 70px 0;
    `}

    .button {
        margin: 20px 0 0;

        ${media.medium`
            width: 192px;
            height: 52px;
            margin: 30px auto 0;
        `}

        ${media.large`
            margin: 50px auto 0;
            width: 222px;
        `}
    }
`;

const Title = styled.h4`${baseCss}`.extend`
    font-family: 'Proxima Nova Extrabold';
    font-size: 24px;
    line-height: 28px;
    color: #212121;

    ${media.medium`
        font-size: 43px;
        line-height: 47px;
    `}

    ${media.large`
        font-size: 47px;
        line-height: 47px;
    `}
`;

const Paragraph = styled.p`${baseCss}`.extend`
    font-family: 'Proxima Nova Regular';
    font-size: 22px;
    line-height: 26px;
    color: #212121;
    margin: 10px 0 0;

    ${media.medium`
        margin: 20px auto 0;
        font-size: 30px;
        line-height: 40px;
        max-width: 740px;
    `}

    ${media.large`
        max-width: 830px;
        font-size: 33px;
        margin: 30px auto 0;
    `}

    ${media.wide`
        max-width: none;
    `}
`;

const Input = styled.input`${baseCss}`.extend`
    width: 100%;
    background: #F9F9F9;
    border: none;
    border-radius: 6px;
    padding: 18px 53px;
    text-align: center;
    box-sizing: border-box;
    color: #212121;
    font-family: 'Proxima Nova Regular';
    font-size: 20px;
    line-height: 23px;
    margin: 30px 0 0;

    input[placeholder], input::-moz-placeholder, input:-moz-placeholder,
    input:-ms-input-placeholder {
        color: rgba(33, 33, 33, 0.5);
        font-family: 'Proxima Nova Regular';
        font-size: 20px;
        line-height: 23px;
    }

    &:focus {
      outline: none;
    }

    ${media.medium`
        max-width: 720px;
        font-size: 28px;
        line-height: 33px;
        padding: 25px 120px;
        margin: 40px 0 0;
    `}

    ${media.large`
        max-width: 940px;
        padding: 35px 230px;
        font-size: 30px;
        line-height: 35px;
    `}

    ${media.wide`
        max-width: 1146px;
        margin: 50px 0 0;
    `}
`;

export { Mail }
