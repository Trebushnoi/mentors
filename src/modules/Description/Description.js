import React, { Component } from 'react';
import balance from './balance.svg';
import stairsteps from './stairsteps.svg';
import { LightButton as Button } from '../Common/Button';
import { baseCss } from '../Common/StyledComponent';
import styled, { ThemeProvider } from 'styled-components';
import { media } from '../Common/Media';

class Description extends Component {
  render() {
    return (
      <Wrapper>
          <Title> <CompanyName> Mentor Forces </CompanyName> — сервис онлайн консультаций один на один с личным наставником. </Title>
          <div className = 'problem-wrapper'>
              <div className = 'problem'>
                  <Problem icon = { stairsteps }
                      title = 'Если ты делаешь первые шаги в дизайне'
                      description = 'Выбери подходящего ментора, длину и дату консультации, и задай все свои вопросы.' />
              </div>

              <div className = 'problem'>
                  <Problem icon = { balance }
                      title = 'Независимый арт-дирекшн или оценка макета'
                      description = 'Нужен взгляд со стороны и пара советов? Выбери дату, длину и время консультации.' />
              </div>
          </div>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`${baseCss}`.extend`
    padding: 40px 15px;

    ${media.medium`
        padding: 50px 0;
        max-width: 720px;
        margin: auto;
    `}

    ${media.large`
        max-width: 940px;
        padding: 70px 0;
    `}

    ${media.wide`
        max-width: 1146px;
    `}

    .problem {
        margin: 40px 0 0;

        ${media.medium`
            margin: 50px 0 0;
        `}

        ${media.large`
            margin: 0;
            margin-right: 48px;
            &:last-child {
                margin-right: 0;
            }
        `}

        ${media.wide`
            margin-right: 50px;
            &:last-child {
                margin-right: 0;
            }
        `}
    }

    .problem-wrapper {
        ${media.large`
            display: flex;
            margin-top: 70px;
        `}
    }
`;

const Title = styled.h1`${baseCss}`.extend`
    font-size: 22px;
    line-height: 26px;
    font-family: 'Proxima Nova Regular';

    ${media.medium`
        font-size: 32px;
        line-height: 47px;
    `}

    ${media.large`
        font-size: 34px;
        line-height: 47px;
    `}

    ${media.wide`
        max-width: 940px;
    `}
`;

const CompanyName = styled.span`${baseCss}`.extend`
    color: #26ae60;
    font-family: 'Proxima Nova Bold';
`;

const Problem = ({icon, title, description }) => (
   <ProblemWrapper>
      <Icon src = { icon } />
      <div className = 'wrapper'>
          <ProblemTitle> {title} </ProblemTitle>
          <Text> {description} </Text>

          <div className = 'button'>
              <a target='_blank' href='http://mentorforces.com/mentors'>
                  <Button text= 'Решить проблему' />
              </a>
          </div>
      </div>
   </ProblemWrapper>
);

const ProblemWrapper = styled.div`${baseCss}`.extend`
    ${media.medium`
        display: flex;
        align-items: flex-start;
    `}

    .button {
        margin-top: 20px;

        ${media.medium`
            width:232px;
            height: 44px;
            margin-top:30px;
        `}

    }

    .wrapper {
        ${media.large`

        `}

        ${media.wide`
            flex-basis: auto;
        `}
    }
`;

const Icon = styled.img`${baseCss}`.extend`
    width: 51px;

    ${media.medium`
        width: 31px;
        margin-right: 32px;
        flex-shrink: 0;
    `}

    ${media.large`
        width: 49px;
        margin-right: 31px;
    `}

    ${media.wide`
        width: 65px;
    `}
`;

const ProblemTitle = styled.h3`${baseCss}`.extend`
    font-size: 20px;
    line-height: 23px;
    margin-top: 20px;
    font-family: 'Proxima Nova Bold';
    color: #212121;

    ${media.medium`
        margin-top:0;
        font-size: 28px;
        line-height: 33px;
    `}

    ${media.large`
        font-size: 29px;
        line-height: 35px;
        display: inline-block;
    `}

    ${media.wide`
        max-width: 430px;
    `}
`;

const Text = styled.p`${baseCss}`.extend`
    font-size: 14px;
    line-height: 21px;
    font-family: 'Roboto Light';
    color: #212121;
    margin-top: 15px;

    ${media.medium`
        margin-top:20px;
        font-size: 16px;
        line-height: 24px;
        display: inline-block;
    `}

    ${media.large`
        max-width: 360px;
    `}

    ${media.wide`
        max-width: 449px;
    `}
`;

export { Description }
