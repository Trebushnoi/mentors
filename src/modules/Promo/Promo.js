import React, { Component } from 'react';
import SvgSprite from 'react-svg-sprite'
import logo from './logo.svg';
import img from './Img.jpg';
import imgWide from './Img-wide.jpg';
import imgLarge from './Img-large.jpg';
import { Button } from '../Common/Button';
import { media } from '../Common/Media';
import styled from 'styled-components';
import { baseCss } from '../Common/StyledComponent';

class Promo extends Component {
  render() {
    return (
      <Wrapper>
          <div className = 'content'>
              <img className = 'logo' src = { logo } />
              <p className = 'paragraph-bold'> Твой личный дизайн-наставник </p>
              <p className = 'paragraph'> Оценка, арт-дирекшн, менторство </p>
              <div className = 'button'>
                  <a target='_blank' href='http://mentorforces.com/mentors'><Button text = 'Получить консультацию' /></a>
              </div>
          </div>
      </Wrapper>
    );
  }
}


const Wrapper = styled.div`${baseCss}`.extend`
    background-color: #27AE60;
    padding: 40px 15px;

    ${media.medium`
        text-align: center;
        padding: 30px 0 50px;
        background-image: url('${img}');
        position:relative;
        background-size: cover;
        margin: 30px 30px 0px 30px;
        background-color: white;

        &:after {
            position: absolute;
            content: '';
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(33, 33, 33, 0.5);
        }
    `}

    ${media.large`
        background-image: url('${imgLarge}');
        padding: 40px 0 70px;
    `}

    ${media.wide`
        background-image: url('${imgWide}');
    `}

    .logo {
        ${media.medium`
            width: 214px;
        `}
    }

    .content {
      ${media.medium`
          position: relative;
          z-index: 1;
      `}
    }

    .button {
        margin: 40px 0 0;

        ${media.medium`
            width: 334px;
            height: 52px;
            margin: 50px auto 0;
        `}

        ${media.large`
            margin: 70px auto 0;
        `}
    }

    .paragraph-bold {
        margin: 40px 0 0;
        font-size: 24px;
        line-height: 28px;
        color: white;
        font-family: 'Proxima Nova Extrabold';
        max-width: 245px;
        ${media.medium`
            max-width: none;
            margin: 80px 0 0;
            font-size: 45px;
            line-height: 1;
        `}

        ${media.large`
            margin: 100px 0 0;
        `}
    }

    .paragraph {
        font-size: 22px;
        line-height: 26px;
        color: white;
        font-family: 'Proxima Nova Regular';
        margin: 10px 0 0;
        ${media.medium`
            font-size: 31px;
            line-height: 1;
            margin: 20px 0 0;
        `}
    }
`;

const ParagraphBold = styled.p`${baseCss}`.extend`
    font-size: 24px;
    line-height: 28px;
    color: white;
    font-family: 'Proxima Nova Extrabold';
    max-width: 245px;
`;

const Paragraph = styled.p`${baseCss}`.extend`
    font-size: 22px;
    line-height: 26px;
    color: white;
    font-family: 'Proxima Nova Regular';
`;

export { Promo }
