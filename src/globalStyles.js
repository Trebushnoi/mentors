import { injectGlobal } from 'styled-components';
import reset from 'styled-reset';

import ProximaNovaExtrabldTTF from './assets/fonts/ProximaNova/Extrabold/ProximaNova-Extrabld.ttf';
import ProximaNovaExtrabldEOT from './assets/fonts/ProximaNova/Extrabold/ProximaNova-Extrabld.eot';
import ProximaNovaExtrabldWOFF from './assets/fonts/ProximaNova/Extrabold/ProximaNova-Extrabld.woff';

import ProximaNovaBoldTTF from './assets/fonts/ProximaNova/Bold/ProximaNova-Bold.ttf';
import ProximaNovaBoldEOT from './assets/fonts/ProximaNova/Bold/ProximaNova-Bold.eot';
import ProximaNovaBoldWOFF from './assets/fonts/ProximaNova/Bold/ProximaNova-Bold.woff';

import ProximaNovaRegularTTF from './assets/fonts/ProximaNova/Regular/ProximaNova-Regular.ttf';
import ProximaNovaRegularEOT from './assets/fonts/ProximaNova/Regular/ProximaNova-Regular.eot';
import ProximaNovaRegularWOFF from './assets/fonts/ProximaNova/Regular/ProximaNova-Regular.woff';

import RobotoLightTTF from './assets/fonts/Roboto/Light/roboto-light.ttf';
import RobotoLightEOT from './assets/fonts/Roboto/Light/roboto-light.eot';
import RobotoLightWOFF from './assets/fonts/Roboto/Light/roboto-light.woff';

import RobotoRegularTTF from './assets/fonts/Roboto/Regular/roboto-regular.ttf';
import RobotoRegularEOT from './assets/fonts/Roboto/Regular/roboto-regular.eot';
import RobotoRegularWOFF from './assets/fonts/Roboto/Regular/roboto-regular.woff';

import RobotoBoldTTF from './assets/fonts/Roboto/Bold/roboto-bold.ttf';
import RobotoBoldEOT from './assets/fonts/Roboto/Bold/roboto-bold.eot';
import RobotoBoldWOFF from './assets/fonts/Roboto/Bold/roboto-bold.woff';

const GlobalStyles = injectGlobal`
  ${reset}
  a {
      text-decoration: none;
      cursor: pointer;
  }

  button {
      cursor: pointer;
  }

  * {
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }

  *:focus {
      outline: none;
  }

  @font-face {
      font-family: 'Roboto Bold';
      src: url('${RobotoBoldEOT}');
      src: local('Roboto Bold'), local('Robot-Bold'),
        url('${RobotoBoldEOT}?#iefix') format('embedded-opentype'),
        url('${RobotoBoldWOFF}') format('woff'),
        url('${RobotoBoldTTF}') format('truetype');
      font-weight: 800;
      font-style: normal;
  }

  @font-face {
      font-family: 'Roboto Regular';
      src: url('${RobotoRegularEOT}');
      src: local('Roboto Regular'), local('Robot-Regular'),
        url('${RobotoRegularEOT}?#iefix') format('embedded-opentype'),
        url('${RobotoRegularWOFF}') format('woff'),
        url('${RobotoRegularTTF}') format('truetype');
      font-weight: 800;
      font-style: normal;
  }

  @font-face {
      font-family: 'Roboto Light';
      src: url('${RobotoLightEOT}');
      src: local('Roboto Light'), local('Robot-Light'),
        url('${RobotoLightEOT}?#iefix') format('embedded-opentype'),
        url('${RobotoLightWOFF}') format('woff'),
        url('${RobotoLightTTF}') format('truetype');
      font-weight: 800;
      font-style: normal;
  }

  @font-face {
    	font-family: 'Proxima Nova Extrabold';
    	src: url('${ProximaNovaExtrabldEOT}');
    	src: local('Proxima Nova Extrabold'), local('ProximaNova-Extrabld'),
    		url('${ProximaNovaExtrabldEOT}?#iefix') format('embedded-opentype'),
    		url('${ProximaNovaExtrabldWOFF}') format('woff'),
    		url('${ProximaNovaExtrabldTTF}') format('truetype');
    	font-weight: 800;
    	font-style: normal;
  }

  @font-face {
      font-family: 'Proxima Nova Bold';
      src: url('${ProximaNovaBoldEOT}');
      src: local('Proxima Nova Bold'), local('ProximaNova-Bold'),
        url('${ProximaNovaBoldEOT}?#iefix') format('embedded-opentype'),
        url('${ProximaNovaBoldWOFF}') format('woff'),
        url('${ProximaNovaBoldTTF}') format('truetype');
      font-weight: 800;
      font-style: normal;
  }

  @font-face {
      font-family: 'Proxima Nova Regular';
      src: url('${ProximaNovaRegularEOT}');
      src: local('Proxima Nova Regular'), local('ProximaNova-Regular'),
        url('${ProximaNovaRegularEOT}?#iefix') format('embedded-opentype'),
        url('${ProximaNovaRegularWOFF}') format('woff'),
        url('${ProximaNovaBoldTTF}') format('truetype');
      font-weight: 800;
      font-style: normal;
  }

`;
