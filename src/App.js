import React, { Component } from 'react';
import { Promo } from './modules/Promo';
import { Description } from './modules/Description';
import { Mentors } from './modules/Mentors';
import { Directions } from './modules/Directions';
import { Prices } from './modules/Prices';
import { Workflow } from './modules/Workflow';
import { Opinions } from './modules/Opinions';
import { Mail } from './modules/Mail';
import { Socials } from './modules/Socials';
import { media } from './modules/Common/Media';
import styled from 'styled-components';
import './globalStyles.js';

class App extends Component {
  render() {
    const { className } = this.props;
    return (
      <div className = {className}>
          <Promo/>
          <Description/>
          <Mentors/>
          <Directions/>
          <Prices/>
          <div className = 'devider'></div>
          <Workflow/>
          <Opinions/>
          <Mail/>
          <Socials/>
      </div>
    );
  }
}

export const StyledApp = styled(App)`
    overflow-x: hidden;
    .devider {
      display: none;

      ${media.large`
          display: block;
          height: 1px;
          background-color: #E8E8E8;
      `}
    }
`;

export default App;
